<?php
return [
    ['GET', '/', function() {
        return load_content_into_main(__DIR__ . '/accueil.php');
    }],
    ['GET', '/contact', function() {
        return load_content_into_main(__DIR__ . '/contact.php');
    }],
];
