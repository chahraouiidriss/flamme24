<?php

header('Content-Type: text/plain');

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$latitude = $_GET['latitude'];
$longitude = $_GET['longitude'];

$stmt = $conn->prepare("INSERT INTO localisations (latitude, longitude) VALUES (?, ?)");
$stmt->bind_param("dd", $latitude, $longitude);
$stmt->execute();

echo "Localisation enregistrée avec succès. Latitude: $latitude, Longitude: $longitude";

$stmt->close();
$conn->close();
?>