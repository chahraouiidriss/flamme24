<?php
header('Content-Type: application/json; charset=utf-8');
header('Content-Disposition: attachment; filename="localisations.json"');

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

// Créer une connexion
$conn = new mysqli($servername, $username, $password, $dbname);

// Vérifier la connexion
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$query = "SELECT localisations.id, localisations.latitude, localisations.longitude, flammes.nom AS nomFlamme FROM localisations JOIN flammes ON localisations.id_flamme = flammes.id ORDER BY localisations.timestamp ASC";
$result = $conn->query($query);

$data = array();
while ($row = $result->fetch_assoc()) {
    $data[] = $row;
}

echo json_encode($data, JSON_PRETTY_PRINT);
$conn->close();
?>
