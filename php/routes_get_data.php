<?php
return [
    ['GET', '/saveLocation', function() {
        require __DIR__ . '/get-localisation.php';
    }],
    ['GET', '/fetchData', function() {
        require __DIR__ . '/fetchData.php';
        $resultJson = fetchData();
        $result = json_decode($resultJson, true);
        if ($result['status'] === 'success') {
            return ['status' => 'success', 'message' => $result['message'], 'data' => $result['data']];
        } else {
            return ['status' => 'error', 'message' => $result['message']];
        }
    }],
    ['GET', '/export', function() {
        include __DIR__ . '/export.php';
    }],
];
