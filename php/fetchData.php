<?php
function fetchData() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "test";

    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        return json_encode(['status' => 'error', 'message' => 'Connection failed: '. $conn->connect_error]);
    }

    $sql = "SELECT localisations.latitude, localisations.longitude, localisations.timestamp, flammes.nom AS nomFlamme, flammes.id AS idFlamme
            FROM localisations 
            JOIN flammes ON localisations.id_flamme = flammes.id 
            ORDER BY localisations.timestamp ASC";
    $result = $conn->query($sql);

    $data = [];
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }
    }
    $conn->close();
    return json_encode(['status' => 'success', 'message' => 'Récupération de la liste avec succès', 'data' => $data]);
}
