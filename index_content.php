<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Site avec Carte et Barre de Recherche</title>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">
            <img src="assets/OIP__2_-removebg-preview.png" alt="Logo 1" class="logo1">
            <img src="assets/paris2024-removebg-preview.png" alt="Logo 2" class="logo2">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <form id="searchForm" class="form-inline ml-auto">
                <div class="input-group">
                    <input type="search" class="form-control input" id="searchBox" placeholder="Recherche...">
                    <div class="input-group-append">
                        <button class="btn btn-light" type="submit" id="searchButton">
                            <span>Rechercher</span>
                        </button>
                    </div>
                </div>
                <select class="form-control ml-3" id="mapStyle">
                    <option value="streets">Streets</option>
                    <option value="satellite">Satellite</option>
                </select>
            </form>
        </div>
    </nav>
    <div id="map" class="my-3"></div>
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <button class="btn my-2" id="downloadDataBtn">Télécharger les données</button>
                <button class="btn my-2" id="showTransportModalBtn">Choisir le mode de transport</button>
            </div>
        </div>
    </div>

    <!-- Modal pour choisir le mode de transport -->
    <div id="transportModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Choisissez le mode de transport</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <select id="transportMode" class="form-control">
                        <option value="driving-car">Voiture</option>
                        <option value="foot-walking">À pieds</option>
                        <option value="cycling-regular">Vélo</option>
                    </select>
                    <div id="routeDetails" class="mt-3"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="confirmTransportMode">Confirmer</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
</body>

</html>
