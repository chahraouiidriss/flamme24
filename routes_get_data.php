<?php
return [
    ['GET', '/fetchData', function() {
        include 'fetchData.php';
    }],
    ['GET', '/export', function() {
        include 'export.php';
    }],
    ['GET', '/recherche', function() {
        include 'recherche.php';
    }],
];
