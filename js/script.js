document.addEventListener('DOMContentLoaded', function () {
    var defaultPosition = [43.94949342, 4.81690407]; // Position par défaut
    var map = L.map('map').setView(defaultPosition, 13);
    var tileLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '© OpenStreetMap contributors',
        maxZoom: 19,
    }).addTo(map);

    const flameColors = {
        1: 'red',
        2: 'black',
    };

    var lastMarker;
    var polyline; // Define polyline here
    var autoFocusEnabled = true;
    var zoomLevel = 13;
    var routeLine;
    var transportMode = 'driving-car'; // Default transport mode

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var currentPosition = [position.coords.latitude, position.coords.longitude];
            map.setView(currentPosition, zoomLevel);
        }, function () {
            map.setView(defaultPosition, zoomLevel);
        });
    } else {
        map.setView(defaultPosition, zoomLevel);
    }

    document.getElementById('searchForm').addEventListener('submit', function (e) {
        e.preventDefault();
        var searchQuery = document.getElementById('searchBox').value;
        fetch(`https://nominatim.openstreetmap.org/search?q=${encodeURIComponent(searchQuery)}&format=json`)
            .then(response => response.json())
            .then(data => {
                if (data.length > 0) {
                    var lat = data[0].lat;
                    var lon = data[0].lon;
                    map.setView([lat, lon], 13);
                } else {
                    alert('Lieu non trouvé');
                }
            })
            .catch(error => console.error('Search error:', error));
    });

    function calculateFlameSpeed(locations) {
        if (locations.length < 2) return 0;

        let totalDistance = 0;
        let totalTime = 0;

        for (let i = 1; i < locations.length; i++) {
            let previous = locations[i - 1];
            let current = locations[i];
            let distance = map.distance([previous.latitude, previous.longitude], [current.latitude, current.longitude]);
            let time = new Date(current.timestamp).getTime() - new Date(previous.timestamp).getTime();

            totalDistance += distance;
            totalTime += time;
        }

        let speed = (totalDistance / 1000) / (totalTime / 3600000); // km/h
        return speed;
    }

    function predictFuturePosition(locations, speed, currentTime, futureTime) {
        if (locations.length === 0) return null;

        let lastLocation = locations[locations.length - 1];
        let distanceToTravel = speed * ((futureTime - currentTime) / 3600000); // en km
        let bearing = getBearing(locations[locations.length - 2], lastLocation);

        let futurePosition = getDestinationPoint(lastLocation.latitude, lastLocation.longitude, distanceToTravel, bearing);
        return futurePosition;
    }

    function getBearing(start, end) {
        let startLat = start.latitude * Math.PI / 180;
        let startLng = start.longitude * Math.PI / 180;
        let endLat = end.latitude * Math.PI / 180;
        let endLng = end.longitude * Math.PI / 180;

        let y = Math.sin(endLng - startLng) * Math.cos(endLat);
        let x = Math.cos(startLat) * Math.sin(endLat) - Math.sin(startLat) * Math.cos(endLat) * Math.cos(endLng - startLng);
        let bearing = Math.atan2(y, x) * 180 / Math.PI;

        return (bearing + 360) % 360;
    }

    function getDestinationPoint(lat, lng, distance, bearing) {
        let radius = 6371e3; // Earth's radius in meters
        let δ = distance / radius; // Angular distance in radians
        let θ = bearing * Math.PI / 180;

        let φ1 = lat * Math.PI / 180;
        let λ1 = lng * Math.PI / 180;

        let φ2 = Math.asin(Math.sin(φ1) * Math.cos(δ) + Math.cos(φ1) * Math.sin(δ) * Math.cos(θ));
        let λ2 = λ1 + Math.atan2(Math.sin(θ) * Math.sin(δ) * Math.cos(φ1), Math.cos(δ) - Math.sin(φ1) * Math.sin(φ2));

        let newLat = φ2 * 180 / Math.PI;
        let newLng = λ2 * 180 / Math.PI;

        return { latitude: newLat, longitude: newLng };
    }

    function calculateDistanceAndTimeToFlame(currentPosition, flamePosition, speed) {
        let distance = map.distance([currentPosition.latitude, currentPosition.longitude], [flamePosition.latitude, flamePosition.longitude]);
        let time = (distance / 1000) / speed * 3600; // Time in seconds

        return { distance, time };
    }

    function getRouteToFlame(currentPosition, flamePosition, mode, callback) {
        if (!flamePosition || !flamePosition.lat || !flamePosition.lng) {
            console.error('Invalid flame position:', flamePosition);
            alert('Aucune position de flamme valide trouvée.');
            return;
        }

        let url = `https://api.openrouteservice.org/v2/directions/${mode}?api_key=5b3ce3597851110001cf6248336067c2b3924705a97b79478c9e3b68&start=${currentPosition.longitude},${currentPosition.latitude}&end=${flamePosition.lng},${flamePosition.lat}`;
        
        fetch(url)
            .then(response => response.json())
            .then(data => {
                if (data.features && data.features.length > 0) {
                    let coordinates = data.features[0].geometry.coordinates.map(coord => [coord[1], coord[0]]);
                    if (routeLine) {
                        map.removeLayer(routeLine);
                    }
                    routeLine = L.polyline(coordinates, { color: 'black' }).addTo(map);

                    let summary = data.features[0].properties.segments[0];
                    let distance = summary.distance / 1000; // km
                    let duration = summary.duration / 60; // minutes

                    // Afficher les détails de l'itinéraire dans le modal
                    document.getElementById('routeDetails').innerHTML = `
                        <p>Distance: ${distance.toFixed(2)} km</p>
                        <p>Durée estimée: ${duration.toFixed(2)} minutes</p>
                    `;

                    if (typeof callback === 'function') {
                        callback();
                    }
                } else {
                    console.error('Invalid route data:', data);
                    alert('Impossible de calculer l\'itinéraire.');
                }
            })
            .catch(error => console.error('Routing error:', error));
    }

    document.getElementById('showTransportModalBtn').addEventListener('click', function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                let currentPosition = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                };
                if (lastMarker) {
                    let flamePosition = lastMarker.getLatLng();
                    transportMode = document.getElementById('transportMode').value; // Set transportMode to the selected value
                    getRouteToFlame(currentPosition, { lat: flamePosition.lat, lng: flamePosition.lng }, transportMode);
                } else {
                    alert('Aucune position de flamme trouvée.');
                }
            });
        } else {
            alert('La géolocalisation n\'est pas supportée par ce navigateur.');
        }
        $('#transportModal').modal('show');
    });

    document.getElementById('confirmTransportMode').addEventListener('click', function () {
        $('#transportModal').modal('hide');
    });

    document.querySelector('.close').addEventListener('click', function () {
        $('#transportModal').modal('hide');
    });

    document.getElementById('mapStyle').addEventListener('change', function () {
        var style = this.value;
        var tileUrl = style === 'streets' ? 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png' : 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}@2x.png';
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        L.tileLayer(tileUrl, {
            attribution: '© OpenStreetMap contributors',
            maxZoom: 19,
        }).addTo(map);
    });

    function fetchFullHistory() {
        fetch('/fetchData')
            .then(response => response.json())
            .then(data => {
                if (data.status === 'success') {
                    var latLngs = data.data.map(location => [location.latitude, location.longitude]);
                    if (latLngs.length > 0) {
                        L.polyline(latLngs, { color: 'blue' }).addTo(map);
                    }
                } else {
                    console.error('Error fetching history:', data.message);
                }
            })
            .catch(error => console.error('Fetch history error:', error));
    }

    function fetchLastPosition() {
        fetch('/fetchData')
            .then(response => response.json())
            .then(data => {
                if (data.status === 'success' && data.data.length > 0) {
                    let lastData = data.data[data.data.length - 1];
                    if (lastMarker) {
                        map.removeLayer(lastMarker);
                    }
                    var flameIcon = L.icon({
                        iconUrl: 'assets/flame-icon.png',
                        iconSize: [38, 95],
                        iconAnchor: [22, 94],
                        popupAnchor: [-3, -76]
                    });
                    lastMarker = L.marker([lastData.latitude, lastData.longitude], { icon: flameIcon }).addTo(map)
                        .bindPopup(lastData.nomFlamme);

                    let latLngs = data.data.map(location => [location.latitude, location.longitude]);
                    if (polyline) {
                        map.removeLayer(polyline);
                    }
                    polyline = L.polyline(latLngs, { color: flameColors[lastData.idFlamme] || 'blue' }).addTo(map);

                    if (autoFocusEnabled) {
                        map.setView([lastData.latitude, lastData.longitude], zoomLevel);
                    }
                } else {
                    console.error('Error fetching last position:', data.message);
                }
            })
            .catch(error => console.error('Fetch location error:', error));
    }

    fetchFullHistory();
    fetchLastPosition();

    setInterval(fetchLastPosition, 60000);

    map.on('click', function (e) {
        saveLocation(e.latlng.lat, e.latlng.lng);
    });

    function saveLocation(latitude, longitude) {
        fetch('/get-localisation', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: `latitude=${latitude}&longitude=${longitude}`
        })
            .then(response => response.text())
            .then(data => {
                notifyUser("Localisation enregistrée avec succès");
            })
            .catch(error => console.error('Save location error:', error));
    }

    function notifyUser(message) {
        var notification = document.createElement('div');
        notification.className = 'notification';
        notification.innerText = message;
        document.body.appendChild(notification);
        setTimeout(function () {
            document.body.removeChild(notification);
        }, 3000);
    }
});
