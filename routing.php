<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
require(__DIR__ . '/vendor/autoload.php');

function cors() {
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');
    }

    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Access-Control-Allow-Headers: X-Requested-With, X-My-App-Request, Content-Type, Accept, Origin, Authorization");
        exit(0);
    }
}

cors();

$dispatcher = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) {
    $r->addRoute('GET', '/', function() {
        ob_start();
        require __DIR__ . '/index_content.php';
        $content = ob_get_clean();
        return $content;
    });

    $getDataRoutes = require __DIR__ . '/php/routes_get_data.php';
    foreach ($getDataRoutes as $route) {
        $r->addRoute($route[0], $route[1], $route[2]);
    }

    $postDataRoutes = require __DIR__ . '/php/routes_post_data.php';
    foreach ($postDataRoutes as $route) {
        $r->addRoute($route[0], $route[1], $route[2]);
    }

    $pagesRoutes = require __DIR__ . '/php/routes_pages.php';
    foreach ($pagesRoutes as $route) {
        $r->addRoute($route[0], $route[1], $route[2]);
    }

    $errorsRoutes = require __DIR__ . '/php/routes_errors.php';
    foreach ($errorsRoutes as $route) {
        $r->addRoute($route[0], $route[1], $route[2]);
    }
});

$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}

$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        header('HTTP/1.1 404 Not Found');
        echo '404 Not Found';
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        header('HTTP/1.1 405 Method Not Allowed');
        echo '405 Method Not Allowed';
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        $output = call_user_func($handler, $vars);
        if (is_array($output)) {
            header('Content-Type: application/json');
            echo json_encode($output);
        } else {
            echo $output;
        }
        break;
}
